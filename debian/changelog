qtwebview-opensource-src (5.11.3-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 16:43:59 -0300

qtwebview-opensource-src (5.11.3-1) experimental; urgency=medium

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 24 Dec 2018 10:44:14 -0300

qtwebview-opensource-src (5.11.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 17 Oct 2018 00:26:57 +0300

qtwebview-opensource-src (5.11.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build dependencies to 5.11.2.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sun, 07 Oct 2018 18:52:31 -0500

qtwebview-opensource-src (5.11.1-3) unstable; urgency=medium

  * Move libqtwebview_webengine.so from libqt5webview5-dev to libqt5webview5
    (closes: #907249).
  * Make qml-module-qtwebview depend on qml-module-qtwebengine.
  * Bump Standards-Version to 4.2.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 31 Aug 2018 23:30:26 +0300

qtwebview-opensource-src (5.11.1-2) unstable; urgency=medium

  * Upload to Sid.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 25 Jul 2018 04:49:33 -0500

qtwebview-opensource-src (5.11.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.1.5, no changes needed.
  * Bump build dependencies to 5.11.1.
  * Bump debhelper compat to 11, no changes needed.
  * Update install file for the new upstream release.
  * Update symbols from amd64 build logs.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 18 Jul 2018 04:40:12 -0500

qtwebview-opensource-src (5.10.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Apr 2018 21:02:03 -0300

qtwebview-opensource-src (5.10.1-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch for the new upstream tarballs names.
  * Bump Qt build-dependencies to 5.10.1.
  * Update Vcs fields to point to salsa.debian.org.
  * Use dh_auto_configure provided by debhelper.
  * Add Qt_5.10 symbol to debian/libqt5webview5.symbols.
  * Bump Standards-Version to 4.1.3, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 15 Mar 2018 12:48:07 +0300

qtwebview-opensource-src (5.9.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.9.2.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 27 Oct 2017 21:40:53 +0300

qtwebview-opensource-src (5.9.1-2) unstable; urgency=medium

  * Bump Standards-Version to 4.1.1, no changes needed.
  * Do not attempt to run tests, they work only with native backends.
  * Use debhelper compat 10 and dh_missing.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 04 Oct 2017 00:22:53 +0700

qtwebview-opensource-src (5.9.1-1) experimental; urgency=medium

  [ Jonathan Riddell ]
  * Initial package. (Closes: #869227)

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 21 Jul 2017 21:59:20 +0300
